# Installation

This plugins is based on both the standard qbehaviour\_studentfeedbackdeferred and the qbehaviour\_asyncevaluation plugins, and needs them to be installed.

# Description

With this question behaviour, questions are automatically graded within adhoc tasks.  
The Quiz page displays a message telling the user the question evaluation is being delayed.  
![A question requiring evaluation on an external server being graded](metadata/screenshots/beinggraded.png)

**Advantages:** When evaluating a question or the whole Quiz, the page will immediately return and not leave the user pending in front of a loading page. This is especially useful when dealing with questions whose evaluation process is not instant.  
**Drawbacks:** The evaluation process may be delayed a bit, waiting for adhoc tasks to be processed (usually every minute, depending on your moodle site settings). It means that some users may have to wait a little bit longer than with another behaviour. It also means that this behaviour is of little use for questions whose evaluation process is instant (e.g. True/False, Multiple choice, etc.).  

# Setup

Once installed, simply go to a Quiz settings > Question behaviour > Select "Asynchronous evaluations (immediate)".

# Deferred feedback version

This plugin also exists in "deferred feedback" version. See the qbehaviour_asyncevaluation plugin.

# About

This software was developed with the Caseine project, with the support of the following organizations:  
- Université Grenoble Alpes  
- Institut Polytechnique de Grenoble

Contributors:
- Astor Bizard
