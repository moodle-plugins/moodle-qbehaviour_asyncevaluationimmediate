<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Question behaviour for the case when students can submit questions one at a time for immediate feedback.
 *
 * The grading is done asynchronously by adhoc tasks. This is useful for questions taking a long time to be evaluated.
 * @package    qbehaviour_asyncevaluationimmediate
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../immediatefeedback/behaviour.php');
require_once(__DIR__ . '/../asyncevaluation/behaviour.php');

/**
 * Question behaviour for deferred feedback with asynchronous evaluation.
 * @see qbehaviour_immediatefeedback
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qbehaviour_asyncevaluationimmediate extends qbehaviour_immediatefeedback {

    /**
     * {@inheritDoc}
     * @see question_behaviour::get_expected_data()
     */
    public function get_expected_data() {
        return array_merge(parent::get_expected_data(), array(
                '_asyncevaluation_pending' => PARAM_BOOL,
                'finish' => PARAM_BOOL, // Also include the standard finish var so we can simulate a finish in asynchronous context.
                'submit' => PARAM_BOOL // Also include the standard submit var so we can simulate a submit in asynchronous context.
        ));
    }

    /**
     * {@inheritDoc}
     * @see question_behaviour::get_expected_qt_data()
     */
    public function get_expected_qt_data() {
        // Override the parent method to allow qt data even when the attempt is finished and the question readonly.
        return $this->question->get_expected_data();
    }

    /**
     * {@inheritDoc}
     * @param question_attempt_pending_step $pendingstep
     * @see qbehaviour_immediatefeedback::process_submit()
     */
    public function process_submit(question_attempt_pending_step $pendingstep) {

        if ($pendingstep->has_behaviour_var('_asyncevaluation_pending')) {
            qbehaviour_asyncevaluation::do_grade_response($pendingstep, $pendingstep->get_qt_data(), $this->question);
            return question_attempt::KEEP;
        }

        if ($this->qa->get_state()->is_finished()) {
            return question_attempt::DISCARD;
        }

        if (!$this->is_complete_response($pendingstep)) {
            $pendingstep->set_state(question_state::$invalid);
        } else {
            $pendingstep->set_state(question_state::$needsgrading);
            $response = $pendingstep->get_qt_data();
            qbehaviour_asyncevaluation::queue_evaluation_task($this->qa, $response, 'submit', $pendingstep->get_user_id());
        }
        return question_attempt::KEEP;
    }

    /**
     * {@inheritDoc}
     * @param question_attempt_pending_step $pendingstep
     * @see qbehaviour_deferredfeedback::process_finish()
     */
    public function process_finish(question_attempt_pending_step $pendingstep) {

        if ($pendingstep->has_behaviour_var('_asyncevaluation_pending')) {
            qbehaviour_asyncevaluation::do_grade_response($pendingstep, $this->qa->get_last_step()->get_qt_data(), $this->question);
            return question_attempt::KEEP;
        }

        if ($this->qa->get_state()->is_finished()) {
            return question_attempt::DISCARD;
        }

        $response = $this->qa->get_last_step()->get_qt_data();

        if (!$this->question->is_gradable_response($response)) {
            $pendingstep->set_state(question_state::$gaveup);
        } else {
            $pendingstep->set_state(question_state::$needsgrading);
            qbehaviour_asyncevaluation::queue_evaluation_task($this->qa, $response, 'finish', $pendingstep->get_user_id());
        }
        $pendingstep->set_new_response_summary($this->question->summarise_response($response));
        return question_attempt::KEEP;
    }
}
